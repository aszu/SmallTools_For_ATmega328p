#!/usr/bin/perl -w
use common::sense;

# WORKAROUND FOR Device::USB stupid bug that caused throwing warnings to console.
# Let it be here. Should not be dangerous.
BEGIN {
   $ENV{CFLAGS} = ''    unless defined $ENV{CFLAGS};
   $ENV{CPPFLAGS} = ''  unless defined $ENV{CPPFLAGS};
   $ENV{LDFLAGS} = ''   unless defined $ENV{LDFLAGS};
};

use List::Util qw( max ) ;

use Data::Dumper;
$Data::Dumper::Sortkeys = 1;

# see /usr/include/usb.h (on most setups) for details... :)
use constant {
USB_REQ_GET_STATUS            =>  0x00,
USB_REQ_CLEAR_FEATURE         =>  0x01,
USB_REQ_SET_FEATURE           =>  0x03,
USB_REQ_SET_ADDRESS           =>  0x05,
USB_REQ_GET_DESCRIPTOR        =>  0x06,
USB_REQ_SET_DESCRIPTOR        =>  0x07,
USB_REQ_GET_CONFIGURATION     =>  0x08,
USB_REQ_SET_CONFIGURATION     =>  0x09,
USB_REQ_GET_INTERFACE         =>  0x0A,
USB_REQ_SET_INTERFACE         =>  0x0B,
USB_REQ_SYNCH_FRAME           =>  0x0C,
USB_TYPE_STANDARD             =>  (0x00 << 5),
USB_TYPE_CLASS                =>  (0x01 << 5),
USB_TYPE_VENDOR               =>  (0x02 << 5),
USB_TYPE_RESERVED             =>  (0x03 << 5),
USB_RECIP_DEVICE              =>  0x00,
USB_RECIP_INTERFACE           =>  0x01,
USB_RECIP_ENDPOINT            =>  0x02,
USB_RECIP_OTHER               =>  0x03,
USB_ENDPOINT_IN               =>  0x80,
USB_ENDPOINT_OUT              =>  0x00,

#/* Error codes */
USB_ERROR_BEGIN               =>  500000,

};


use Device::USB;

our $DEVICE_VID = 0x6666;
our $DEVICE_PID = 0x30ae;

my $usb;
my $dev;
eval {
   $usb = Device::USB->new;
   $dev =$usb->find_device( $DEVICE_VID, $DEVICE_PID )
      or die "Unable to find device!";

   $dev->open() or die " ups ";
   #printf STDERR "Device: %04X:%04X opened.\n", $dev->idVendor(), $dev->idProduct();
};

my $counter = 10;

$ARGV[0] = uc $ARGV[0];

unless ($ARGV[0]) {

print <<EOH;
PIC18F2550 writer for ATMegas.

This is PIC18F2550 writer - the software written to let write the firmware
to the pickit 2 clone. Licence not decided yet. Consider this software
to be in public domain. It's so bad, that I do not insists on giving the
credits to me.

Avaliable commands (at least some of them - see code)

- project-related commands - controls PIC part attached to
   PIC_SIGNATURE  - read PIC signature
   PIC_RD_CONFIG  - read PIC config (write config to buffer)
   PIC_RD_ID      - read PIC identification (write id to buffer)
   PIC_SET_CONFIG - write PIC config (from buffer)
   PIC_SET_ID     - write PIC id (from buffer)
   PIC_BULK_ERASE - bulk erase PIC
  *PIC_ERASE_AREA - erase just one area of the PIC
   PIC_SET_CODE_ADDRESS - sets the U&H part of the address to write/read code
   PIC_GET_CODE_ADDRESS - gets the U&H part of the address to write/read code
   PIC_WRITE_CODE - write buffer to code area
   PIC_READ_CODE  - read code to buffer area
  *PIC_POWER_ON   - powers on PIC  (not much current available!!!)
  *PIC_POWER_OFF  - powers off PIC (driven directly off atmega port!!!)
  *PIC_ERASE_AREA - erases specified area of the chip
   PIC_HEX_THROW  - throws a hex to the chip

   *notice: PIC_WRITE_CODE and PIC_READ_CODE uses two requests, but that's
    not important for user.
   *notice: commands with star in front of them may be unimplemented. :)

- generic commands - controls used Atmega part (ports, buffer, etc.)

   R           - read buffer content
   W           - write buffer content
   C           - clear buffer content (reads low-endian buffer size)
   PW          - port write
   PS          - port set (bits)
   PC          - port clear (bits)
   PT          - port toggle (bits)
   PR          - port read
   DEVSIG      - get device signature
   DEVBSIZE    - get buffer size (see R and W commands) (low-endian value)

If command needs parameters, it will complain. If not - it will just
be executed. Use hexdump to see better. And remember, AVR is low-endian :)

EOH

} else {
   my $command = shift @ARGV;

   if ($command eq 'R') {
      my ($index, $count) = ($ARGV[0], $ARGV[1]);
      if (defined $count) {
         $index = oct($index) if $index =~ /^0/;
         $count = oct($count) if $count =~ /^0/;
         print STDERR "Reading $count bytes starting from index $index\n";
         dataprint( scalar usbControlReadLarge($dev, 0x02, 0, $index, $count, 1000) );
      } else {
         print "read data binary (R) usage: $0 R index count\n";
      }
   } elsif ($command eq 'W') {
         if (defined $ARGV[1]) {
            my $data = '';
            my $index = shift @ARGV;
            $index = oct($index) if $index =~ /^0/;
            my $item;
            while (defined( $item = shift @ARGV)) {
               $item = oct($item) if $item =~ /^0/;
               $data .= chr($item);
            }
            print STDERR "Writing ".length($data)." bytes starting at index $index\n";
            device_write_buffer_data($dev, $index, $data);
         } else {
            print "write data binary (W) usage: $0 W index <data>, where <data> is series of numbers to be written\n";
         }

   } elsif ($command eq 'C') {
      printf "%s\n", device_perform_buffer_clear($dev, parseNum($ARGV[0],0));
   } elsif ($command =~ /^P[WSCT]$/) {
      if (defined $ARGV[1]) {
         my $port = parseNum(shift @ARGV);
         my $data = parseNum(shift @ARGV);
         my $cmds = {
            PW => 0x81,
            PS => 0x82,
            PC => 0x83,
            PT => 0x84
         };
         usbControlWrite($dev, $$cmds{$command}, $data, $port, undef, 0);
      } else {
         print "port value/set/clear/toggle (P[WSCT]) usage: $0 Px IOAddr byte_value, where IOAddr is address of the port WITH offset and byte_value is the value to set/ mask to operation\n";
      }
   } elsif ($command eq 'PR') {
      if (defined $ARGV[0]) {
         my $port = parseNum(shift @ARGV);
         dataprint ( scalar(usbControlRead($dev, 0xC1, 0, $port, 1)) );
      } else {
         print "io port read (PR) usage: $0 PR IOAddr\n";
      }
   } elsif ($command eq 'DEVSIG') {
      dataprint ( scalar(usbControlRead($dev, 0x72, 0, 0, 4)) );
   } elsif ($command eq 'DEVBSIZE') {
      printf "Buffer size: %s\n",device_get_buffer_size($dev);
   } elsif ($command eq 'PIC_SIGNATURE') {
      dataprint ( scalar(usbControlRead($dev, 0x41, 0, 0, 2)) );
   } elsif ($command eq 'PIC_RD_CONFIG') {
      if (defined $ARGV[2]) {
         dataprint ( scalar(usbControlRead($dev, 0x42, parseNum($ARGV[0]), parseNum($ARGV[1]), parseNum($ARGV[2]))) );
      } else {
         print "Read config to buffer, Usage $0 $command start_config_byte buffer_index count. \nIt reads count bytes from config area (starting from start_config_byte) to buffer, starting at index buffer_index\n";
      }
   } elsif ($command eq 'PIC_RD_ID') {
      if (defined $ARGV[2]) {
         dataprint ( scalar(usbControlReadLarge($dev, 0x43, parseNum($ARGV[0]), parseNum($ARGV[1]), parseNum($ARGV[2]))) );
      } else {
         print "Read device id to buffer, Usage $0 $command start_id_byte buffer_index count. \nIt reads count bytes from device custom id area (starting from start_id_byte) to buffer, starting at index buffer_index\n";
      }
   } elsif ($command eq 'PIC_SET_ID') {
      if (defined $ARGV[2]) {
         dataprint ( scalar(usbControlRead($dev, 0x53, parseNum($ARGV[0]), parseNum($ARGV[1]), parseNum($ARGV[2]))) );
      } else {
         print "Write device id from buffer, Usage $0 $command start_id_byte buffer_index count. \nIt writes count (must be divisible by 2!) bytes from buffer (at offset buffer_index) to device custom id area (first id element to be written is start_id_byte)\n";
      }
   } elsif ($command eq 'PIC_SET_CONFIG') {
      if (defined $ARGV[2]) {
         dataprint ( pic_perform_write_config($dev, parseNum($ARGV[0]), parseNum($ARGV[1]), parseNum($ARGV[2])) );
      } else {
         print "Write device config from buffer, Usage $0 $command start_id_byte buffer_index count. \nIt writes count (must be divisible by 2!) bytes from buffer (at offset buffer_index) to device custom id area (first id element to be written is start_id_byte)\n";
      }
   } elsif ($command eq 'PIC_SET_CODE_ADDRESS') {
      if (defined $ARGV[0]) {
         dataprint (pic_perform_setting_code_address($dev, parseNum($ARGV[0])));
      } else {
         print "Set device's extended base address to write code. Usage $0 $command extended_address_part\n";
      }
   } elsif ($command eq 'PIC_RD_CODE_ADDRESS') {
      dataprint (pic_perform_getting_code_address($dev, parseNum($ARGV[0])));
   } elsif ($command eq 'PIC_WRITE_CODE') {
      if (defined $ARGV[2]) {
         printf "%s\n", pic_perform_write_code($dev, parseNum($ARGV[0]), parseNum($ARGV[1]), parseNum($ARGV[2]) );
      } else {
         print "Write code from buffer, Usage $0 $command start_id_byte buffer_index count. \nIt writes count (must be divisible by 2!) bytes from buffer (at offset buffer_index) to device code area (first id element to be written is start_id_byte)\n";
      }
   } elsif ($command eq 'PIC_READ_CODE') {
      if (defined $ARGV[2]) {
         usbControlRead($dev,0x57, parseNum($ARGV[2]),0,2);
         dataprint ( scalar(usbControlRead($dev, 0x56, parseNum($ARGV[0]), parseNum($ARGV[1]), 2)) );
      } else {
         print "Read code to buffer, Usage $0 $command start_id_byte buffer_index count. \nIt reads count bytes from devicecode area (starting from start_id_byte) to buffer, starting at index buffer_index\n";
      }
   } elsif ($command eq 'PIC_POWER_ON') {
      usbControlRead($dev, 0x5e, 0, 0, 1);
   } elsif ($command eq 'PIC_POWER_OFF') {
      usbControlRead($dev, 0x5f, 0, 0, 1);
   } elsif ($command eq 'PIC_BULK_ERASE') {
      pic_perform_bulk_erase($dev);
   } elsif ($command eq 'PIC_ERASE_AREA') {
      if ($ARGV[0]) {
         # dataprint ( scalar(usbControlRead($dev, 0x61, parseNum($ARGV[0]), 0, 1)) );
      } else {
         print "Erase portion of the chip. Following options are selectable:
               0x3f8f   - chip erase
               0x0084   - data EEPROM
               0x0081   - boot block
               0x0082   - configuration bits
               0x0180   - Code EEPROM Block 0
               0x0280   - Code EEPROM Block 1
               0x0480   - Code EEPROM Block 2
               0x0880   - Code EEPROM Block 3
               0x1080   - Code EEPROM Block 4
               0x2080   - Code EEPROM Block 5\n";
      }
   } elsif ($command eq 'PIC_HEX_THROW') {
      if ($ARGV[0]) {
         pic_throw_hex_file($dev, $ARGV[0]);
      } else {
         print "Throws a hex file to a PIC processor. Maybe. Usage $0 $command hex_File.\n";
      }
   } elsif ($command eq 'PIC_HEX_VERIFY') {
      if ($ARGV[0]) {
         pic_verify_hex_file($dev, $ARGV[0]);
      } else {
         printf "Throws a hex file to a PIC processor. Maybe. Usage %s %s hex_File.\n", $0, $command;
      }
   } else {
      print "no such command! ($command)\n";
   }

}

sub pic_verify_hex_file {
   return pic_throw_hex_file(shift, shift, 1);
}

# Throw a hex file to a pic
# Params:
#  - hex file name.
sub pic_throw_hex_file {
   my ($dev, $fname, $verify) = @_;

   my @lines = hex_get_content($fname);
   my $BUFSIZE = device_get_buffer_size($dev)>>($ENV{BUFSIZEREDUCTION}||0);

   #die "HEX FILE LOADED!";
   if (!$verify) {
      printf "Programming: bulk erase\n";
      pic_perform_bulk_erase($dev);
   }

   pic_perform_setting_code_address($dev, 0);

   my $ExtendedAddress = 0;
   my $BufferWindow = 0; #address of buffer 0 byte
   my $BufferContentSize = 0;
   device_perform_buffer_clear($dev, 0xff);

   use Data::Dumper;
   #print Dumper(\@lines);

   my $niceNcool = 1;
   do {
      my $line = shift @lines;
      if ($$line{Type} =~ /^0?((0)|(2)|(4))$/) {
         if ($$line{Type} == 0) {
            my $Address = $$line{Address} + $ExtendedAddress;

            my $buffer = hex_bufferize_data($$line{Data});

            if (
               (($Address + length $buffer) > ($BufferWindow + $BUFSIZE)) || # data will jump pass the buffer
               ($Address < $BufferWindow)                                     # data before buffer
            ){
               if ($BufferContentSize > 0) {
                  #write buffer to device or verify the buffer
                  if (!$verify) {
                     hex_write_buffer_content($dev, $BufferWindow, $BufferContentSize);
                  } else {
                     hex_verify_buffer_content($dev, $BufferWindow, $BufferContentSize);
                  }
                  #and reset buffer
                  device_perform_buffer_clear($dev, 0xff);
               }
               $BufferWindow = $Address;
               $BufferContentSize = 0;
            }

            $BufferContentSize = max(($Address + length $buffer)-$BufferWindow, $BufferContentSize);
            device_write_buffer_data($dev, $$line{Address}-$BufferWindow, $buffer);

         } elsif ($$line{Type} == 2) {
            die "Currently Extended Segment Address is not implemented.";
            $niceNcool = 0;
         } elsif ($$line{Type} == 4) {
            if ($BufferContentSize > 0) {
               #write buffer to device or verify the buffer
               if (!$verify) {
                  hex_write_buffer_content($dev, $BufferWindow, $BufferContentSize);
               } else {
                  hex_verify_buffer_content($dev, $BufferWindow, $BufferContentSize);
               }
               device_perform_buffer_clear($dev, 0xff);
            }
            $ExtendedAddress = hex('0x'.$$line{Data})<<16;
            $BufferWindow = $ExtendedAddress;
            $BufferContentSize = 0;
         }
      } else {
         if ($BufferContentSize) {
            #write buffer to device or verify the buffer
            if (!$verify) {
               hex_write_buffer_content($dev, $BufferWindow, $BufferContentSize);
            } else {
               hex_verify_buffer_content($dev, $BufferWindow, $BufferContentSize);
            }
            device_perform_buffer_clear($dev, 0xff);
            $BufferContentSize = 0;
         } else {
         }
         #print "DIE DIE DIE ($$line{Type})\n";
         #$niceNcool = 0;

      };
   } while (@lines && $niceNcool);
   if ($verify) {
      print "flash verification OK\n";
   }

}

sub hex_bufferize_data {
   my ($data) = @_;
   my $buffer = '';
   while ($data) {
      my $tmp = substr $data, 0, 2;
      $data = substr $data, 2;
      #print $tmp;
      $tmp = hex("0x$tmp");
      $buffer.=chr($tmp);
   }
   return $buffer;
}

sub hex_write_buffer_content {
   my ($dev, $BufferWindow, $size) = @_;
   if ($BufferWindow < 0x300000) {
      #dataprint( scalar usbControlReadLarge($dev, 0x02, 0, 0, $size, 1000) );
      printf "Programming: writing to 0x%06X (0x%04X bytes - %s).\n",$BufferWindow, $size, $BufferWindow<0x200000?'code':' id ';
      pic_perform_setting_code_address($dev, (($BufferWindow & 0xFFFF00) >> 8));
      pic_perform_write_code($dev, $BufferWindow & 0xFF, 0, $size);
   } else {
      printf "Programming: writing to 0x%06X (0x%04X bytes - conf).\n",$BufferWindow, $size;
      if ($BufferWindow == 0x300000) {
         pic_perform_write_config($dev,$BufferWindow & 0xFF, 0, $size);
      }
   }
}

sub hex_verify_buffer_content {
   my ($dev, $BufferWindow, $size) = @_;
   if ($BufferWindow < 0x300000) {
      #dataprint( scalar usbControlReadLarge($dev, 0x02, 0, 0, $size, 1000) );

      printf "Performing check at 0x%06X, size 0x%04X (%s)\n", $BufferWindow, $size, $BufferWindow<0x200000?'code':' id ';;

      pic_perform_setting_code_address($dev, (($BufferWindow & 0xFFFF00) >> 8));
      my $result = pic_perform_verify_code($dev, $BufferWindow & 0xFF, 0, $size);
      if ($result != 0xFFFF) {
         if ($BufferWindow < 0x200000) {
            die sprintf "ERROR: Verification not OK at code address 0x%06X\n", $BufferWindow + $result;
         } else {
            printf STDERR "WARNING: Verification not OK at address 0x%06X. This is not critical - probably this is chip ID...\n", $BufferWindow + $result;
         }
      }
   } else {
      if ($BufferWindow == 0x300000) {
         print STDERR "\n** notice ** Config verification is not implemented.\n             Although this should not be a big problem, if something is wrong - do a manual check.\n\n";
         #pic_perform_write_config($dev,$BufferWindow & 0xFF, 0, $size);
      }
   }
}

sub pic_perform_write_config {
   my ($dev, $offset, $buffer_idx, $size) = @_;
   return scalar(usbControlRead($dev, 0x52, $offset, $buffer_idx, $size));
}

sub hex_get_content {
   my $fname = shift;

   open HEX, "<$fname" or die "No such file: $fname";

   my @lines = ();
   while (my $line = <HEX>) {
      chomp $line;

      my ($starter, $payloadsize, $address, $recordtype, $data, $checksum) = $line =~ /^(.)(..)(....)(..)(.*)(..)$/;
      unless($starter eq ':') {
         next;
      }

      if (0.5*length($data) != hex("0x$payloadsize" )) {
         die "Payload not matches payload size. expected: ".(hex("0x$payloadsize"))." while received ".(length($data)/2)." for line ($line)!";
      }
      if (my $chk = hex_checksum_check($line)) {
         die sprintf "Checksum not correct for line %s (expected 0x%02X)\n", $line, (hex(substr $line, -2) - $chk)&0xFF ;
      }
      push @lines, {Type=> $recordtype, Address=>hex("0x".$address), Data=> $data};
   }

   close HEX;
   return @lines;
}

sub hex_checksum_check {
   my $line = substr shift, 1;
   my $accumulator = 0;
   my $chk = '';
   while ($line) {
      $accumulator += hex ("0x".substr($line, 0, 2));
      $line = substr $line, 2;
   }
   return $accumulator & 0xFF;
}

sub pic_perform_write_code {
   my ($dev, $offset, $buffer_idx, $size) = @_;
   usbControlRead($dev,0x57, $size,0,2);
   my @result = usbControlRead($dev, 0x55, $offset, $buffer_idx, 2);
   return $result[1]*256+$result[0];
}

sub pic_perform_verify_code {
   my ($dev, $offset, $buffer_idx, $size) = @_;
   usbControlRead($dev,0x57, $size,0,2);
   my @result = usbControlRead($dev, 0x59, $offset, $buffer_idx, 2);
   return $result[1]*256+$result[0];
}

sub pic_perform_bulk_erase {
   my ($dev) = @_;
   my @result = usbControlRead($dev, 0x60, 0, 0, 8);
   return undef;
}

sub pic_perform_setting_code_address {
   my ($device, $address) = @_;
   return scalar(usbControlRead($dev, 0x54, $address, 0, 2));
}

sub pic_perform_getting_code_address {
   my ($device) = @_;
   return scalar(usbControlRead($dev, 0x58, 0, 0, 2));
}

sub device_write_buffer_data {
   my ($dev, $index, $data) = @_;
   return usbControlWrite($dev, 0x01, 0, $index, $data, length($data), 1000);
}

sub device_get_buffer_size {
   my ($dev) = @_;
   my ($result, @result) = (undef, usbControlRead($dev, 0x71, 0, 0, 4));
   $result = $result[1]*256+$result[0];
   return $result;
}

sub device_perform_buffer_clear {
   my ($dev, $char) = @_;
   my @result = usbControlRead($dev, 0x03, $char || 0, 0, 2);
   return $result[1]*256+$result[0];
}

sub asData {
   my ($str, $len) = @_;

   my @data = split (//, $str);

   foreach my $item (@data) {
      $item = ord($item);
   }

   return @data;
}


sub dataprint {
   my ($data) = @_;
   my @data = split (//, $data);
   foreach (@data) {
      $_ = ord($_);
   }

   if (-t STDOUT) {
      my $byte = 0;
         while (@data) {
            my (@part1, @part2);
            printf "%08x  ", $byte;
            my $line = '';
            @part1 = splice @data, 0, 8;
            $byte += @part1;
            foreach (@part1) {
               $line .= (sprintf "%02x ", $_);
            }
            $line .= " ";
            @part2 = splice @data, 0, 8;
            $byte += @part2;
            foreach (@part2) {
               $line .= sprintf "%02x ", $_;
            }
            $line .= (' 'x(50 - length $line));
            print $line;
            print "|";
            foreach (@part1, @part2) {
               $_ = chr($_);
               if (/[\x21-\x7E]/) {
                  print
               } else {
                  print '.'
               }
            }
            print "|\n";
         }
         printf "%08x\n", $byte;
   } else {
      print $data;
   }

}

sub parseNum {
   my $num = shift;
   unless (defined($num)) {
      $num = shift;
   };
   $num = oct($num) if $num =~ /^0/;
   return $num;
}

#eval {
#   print "\n TRYING WRITE \n";
#   print usbControlWrite($dev, 0x01, 0x00, 0x00, "ALAMAKOTA", 8,  1000);
#   print "\n";
#} or print "ERR: $@";


# Read large chunk of data. Calls multiple usbControlRead, increments index.
sub usbControlReadLarge {
   my ($dev, $request, $value, $index, $size, $timeout) = @_;
   my $result = '';

   while ($size > 0) {
      $result .= usbControlRead($dev, $request,$value, $index, $size>0xFE?0xFE:$size, $timeout);
      $index += 0xFE;
      $size -= 0xFE;
   }

   return $result;
}


# Read data from device in single request. Limited to 0xfe bytes.
sub usbControlRead {
   my ($dev, $request, $value, $index, $size, $timeout) = @_;

   if ($size > 0xFE) {
      die "Maximum read size is 0xFE. Decrease size, please.";
   } elsif ($size < 0) {
      $size = -$size;
      print "ASKING SIZE $size\n";
   }

   my $buf;
   $buf = "\0" x $size;

   my $res =   $dev->control_msg(
      USB_TYPE_CLASS | USB_RECIP_DEVICE | USB_ENDPOINT_IN, #request type
      $request,     # request
      $value,       # value
      $index,       # index
      $buf,
      $size,
      $timeout || 1000
   );

   my $buffer = substr($buf, 0, $res);
   if ($res >= 0) {
      if (wantarray) {
         my @result = split('',$buffer);
         foreach (@result) {
            $_ = ord($_);
         }
         return @result;
      } else {
         return $buffer;
      }
   } else {
      die "Error while performing control read ($res)";
   }
}

# Write data to device.
sub usbControlWrite {
   my ($dev, $request, $value, $index, $data, $size, $timeout) = @_;

   my $buf;
   $buf = substr $data, 0, $size;
   $buf .= ("A" x ($size- length $buf));

   my $res =   $dev->control_msg(
      USB_TYPE_CLASS | USB_RECIP_DEVICE | USB_ENDPOINT_OUT, #request type
      $request,     # request
      $value,       # value
      $index,       # index
      $buf,
      $size,
      $timeout || 1000
   );

   my $buffer = substr($buf, 0, $res);

   if ($res >= 0) {
      if (wantarray) {
         return ($buffer, $res);
      } else {
         return $buffer;
      }
   } else {
      die "Error while performing control write ($res)";
   }
}




sub interruptRead {

   my ($dev) = @_;

      my $xres;
      my $buffer = '        ';

      $xres = $dev->interrupt_read(
         1, # endpoint no
         $buffer,
         8,
         500
         );

      $buffer = susbstr($buffer, 0, $xres);
      if (wantarray) {
         return ($buffer, $xres);
      } else {
         if ($xres >= 0) {
            return $buffer;
         } else {
            die "Error while reading interrupt";
         }
      }

}

__END__

