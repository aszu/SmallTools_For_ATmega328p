#!/usr/bin/perl -w

use common::sense;

use Data::Dumper;
$Data::Dumper::Sortkeys = 1;

#print STDERR Dumper(\@ARGV);
#print STDERR $ENV{COMP_LINE}."\n";;

my @commands = (
   R => 'Read buffer (index, count)',
   W => 'Write to buffer (index, [data, ...])',
   C => 'Clear the buffer.',
   PR => 'Read value of IO port or data memory (memAddr)',
   PW => 'Write value to IO port or data memory (memAddr, byte)',
   PS => 'Set    bits of IO port or data memory (memAddr, byte)',
   PC => 'Clear  bits of IO port or data memory (memAddr, byte)',
   PT => 'Toggle bits of IO port or data memory (memAddr, byte)',
   DEVSIG => 'Get device signature',
   DEVBSIZE => 'Get device buffer size',
   PIC_SIGNATURE        => "Returns PIC device's signature bytes",
   PIC_RD_CONFIG        => "Reads PIC's configuration to buffer (offset, buffer_index, length).",
   PIC_RD_ID            => "Reads PIC's id area to buffer (offset, buffer_index, length).",
   PIC_SET_CONFIG       => "Writes buffer to PIC's configuration (offset, buffer_index, length).",
   PIC_SET_ID           => "Writes buffer to PIC's id area (offset, buffer_index, length).",
   PIC_BULK_ERASE       => "Performs PIC bulk erase.",
#  *PIC_ERASE_AREA       =>
   PIC_SET_CODE_ADDRESS => "Sets the U and H bytes of code memory write/read address (extended_address).",
   PIC_RD_CODE_ADDRESS  => "Gets the U and H bytes of code memory write/read address (extended_address).",
   PIC_WRITE_CODE       => "Writes buffer to code area (offset, buffer_index, count).",
   PIC_READ_CODE        => "Reads code area to buffer (offset, buffer_index, count).",
   PIC_HEX_THROW        => "Writes Intel HEX file to PIC.",
   PIC_HEX_VERIFY       => "Verifies hex file against PIC (code only)",
   PIC_POWER_ON         => "Powers on the PIC solution attached to Writer. Remember - DIRECT DRIVE FROM PORT!",
   PIC_POWER_OFF        => "Powers off the PIC attached to Writer.",
#  *PIC_ERASE_AREA       =>
   );

my %commands = @commands;
@commands = grep {$commands{$_}} @commands;

#print STDERR $ARGV[1];

my @opts = grep {$_ =~ /^$ARGV[1]/i} @commands;

my $compLine = $ENV{COMP_LINE};
my $compIndex = $ENV{COMP_POINT};

my $effectiveLine = substr($compLine, 0, $compIndex);


push @opts; #, qw(compgen -f);

my @result;

if (@opts) {

   my $manual = '';

   foreach (@opts) {
     $manual .= sprintf "%-15s- %s\n", $_, $commands{$_};
     push @result, $_;
   }

   if ($ENV{COMP_TYPE} == 63) {
      print STDERR "\n\n".$manual;

   }

}

print join ("\n", @result);

