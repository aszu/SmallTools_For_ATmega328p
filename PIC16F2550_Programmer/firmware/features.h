/*
 * features.h
 *
 *  Created on: Jul 17, 2014
 *      Author: andrzejsz
 */

#ifndef FEATURES_H_
#define FEATURES_H_


#define HAS_PIC_INTERACTIONS
/* Define this to let firmware interact with PIC µC
 *
 */

#define HAS_REGISTER_WRITE
/* Add support for data space write. This command is intended for writing IO Registry
 * file, but it is not limited to in any way (excluding pointer size). You got that
 * command, you own data space.
 */

#define HAS_REGISTER_READ
/* Nothing in data space is a mystery since including this command. Is intended to read
 * IO Registry file, but it is not limited to it in any way (maybe excluding pointer size
 * that is a two byte word).
 */


#endif /* FEATURES_H_ */
