/*
 * main.c
 *
 *  Created on: Jun 21, 2014
 *      Author: andrzejsz
 */

#include <avr/io.h>
#include <stdlib.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <avr/boot.h>
#include <string.h>
//#include "usbdrv/usbdrv.h"

#include <avr/pgmspace.h>   /* required by usbdrv.h */
#include "usbdrv/usbdrv.h"

#include "features.h"

#include "picif.h"

/* CONFIGURATION */

#define MEMORY_BUFFER_SIZE 512
/* Size of the memory buffer to store data in. You definitely don't want to set
 * here too much. Just keep this on the level you require, remember your part's
 * RAM size */

/* ===========================================================================
 * SEE FEATURES.H FOR IMPORTANT CONFIGURATION!
 * ===========================================================================
 */

/* ------------------------------------------------------------------------- */
/* ----------------------------- USB interface ----------------------------- */
/* ------------------------------------------------------------------------- */

/* Commands:
 * 0x01 - WRITE TO BUFFER (value is buffer address, index is size);
 * 0x02 - READ FROM BUFFER (value is buffer address, index is size);
 */
#define REQUEST_WRITE_TO_BUFFER        0x01
#define REQUEST_READ_FROM_BUFFER       0x02
#define REQUEST_CLEAR_BUFFER           0x03

// device requests
#define REQUEST_DEVICE_GET_BUFFER_SIZE 0x71
#define REQUEST_DEVICE_GET_SIGNATURE   0x72

#define REQUEST_EXECUTE_COMMAND_MASK   0x80
//commands operating on device
#define REQUEST_COMMAND_PORT           0x81
#define REQUEST_COMMAND_PORT_SET       0x82
#define REQUEST_COMMAND_PORT_CLR       0x83
#define REQUEST_COMMAND_PORT_TGL       0x84

#define REQUEST_COMMAND_PORT_READ      0xC1

// domain requests
#define REQUEST_PIC_GET_SIGNATURE      0x41
#define REQUEST_PIC_GET_CONFIG         0x42
#define REQUEST_PIC_GET_IDENTIFICATION 0x43

#define REQUEST_PIC_SET_CONFIG         0x52
#define REQUEST_PIC_SET_IDENTIFICATION 0x53
#define REQUEST_PIC_SET_CODE_ADDRESS   0x54
#define REQUEST_PIC_WRITE_CODE         0x55
#define REQUEST_PIC_READ_CODE          0x56
#define REQUEST_PIC_SET_CODE_RW_SIZE   0x57
#define REQUEST_PIC_GET_CODE_ADDRESS   0x58
#define REQUEST_PIC_VERIFY_CODE        0x59

#define REQUEST_PIC_POWER_ON           0x5E
#define REQUEST_PIC_POWER_OFF          0x5F

#define REQUEST_PIC_BULK_ERASE         0x60
#define REQUEST_PIC_AREA_ERASE         0x61

/*********************************************/

uint8_t buffer[MEMORY_BUFFER_SIZE];

/* WORKAROUND:
 * for some reason, when using usbFunctionWrite i had to keep usbRequest structure away
 * as it was overwritten (probably by library) in some funny place between calls. The behaviour
 * was not (or at least seemed not regular) - but in general it looked like in the even calls
 * of usbFunctionWrite rq was replaced with data buffer content.
 * In general, I'm going to try to pin down that problem in a library, but not now :).
 */
usbRequest_t rq;

volatile uint16_t usbTransferProgress = 0;
volatile uint16_t picCodeWriteSize = 0;

uchar usbFunctionWrite(uchar *data, uchar len) {
   uchar * tmp;
   uchar i;

   if (rq.bRequest == REQUEST_WRITE_TO_BUFFER) {
      // this only partially protects range. It is against an accident, not an malicious user.
      if (rq.wIndex.word + usbTransferProgress + len > MEMORY_BUFFER_SIZE) {
         return 0xff;
      }

      tmp = &buffer[rq.wIndex.word + usbTransferProgress];
      i = 0;
      if (len > 0) {
         while (i < len) {
            *tmp = *data;
            data++;
            tmp++;
            i++;
         }
         usbTransferProgress += i;
      }
//      rq.wLength.word -= len;
      return !(usbTransferProgress < (rq.wLength.word));
   }
   return 0xFF;
}

uchar usbFunctionRead(uchar *data, uchar len) {
   uchar * tmp;
   uchar i;
   if (rq.bRequest == REQUEST_READ_FROM_BUFFER) {
      if (rq.wIndex.word + usbTransferProgress + len > MEMORY_BUFFER_SIZE) {
         return 0xff;
      }
      tmp = &buffer[rq.wIndex.word + usbTransferProgress];
      i = 0;
      if (len > 0) {
         while (i < len) {
            *data = *tmp;
            data++;
            tmp++;
            i++;
         }
         usbTransferProgress += i;
      }
      return i;
   } else if (rq.bRequest == REQUEST_CLEAR_BUFFER) {
      for (uint16_t i = 0; i < MEMORY_BUFFER_SIZE; i++) {
         buffer[i] = rq.wValue.bytes[0];
      }
      *(uint16_t *) data = MEMORY_BUFFER_SIZE;
      return 2;
   } else if (rq.bRequest == REQUEST_DEVICE_GET_SIGNATURE) {
      for (i = 0; i < 3; i++) {
         data[0] = SIGNATURE_0;
         data[1] = SIGNATURE_1;
         data[2] = SIGNATURE_2;
         return 3;
      }
      return i;
   } else if (rq.bRequest == REQUEST_DEVICE_GET_BUFFER_SIZE) {
      *(uint16_t*) data = (uint16_t) MEMORY_BUFFER_SIZE;
      return 2;
#  ifdef HAS_PIC_INTERACTIONS
   } else if (rq.bRequest == REQUEST_PIC_GET_SIGNATURE) {
      *((uint16_t*) data) = PIC_read_signature();
      return 2;
   } else if (rq.bRequest == REQUEST_PIC_SET_CODE_ADDRESS) {
      *(uint16_t*) data = PIC_set_code_base_address(rq.wValue.word);
      return 2;
   } else if (rq.bRequest == REQUEST_PIC_GET_CODE_ADDRESS) {
      *(uint16_t*) data = PIC_get_code_base_address();
      return 2;
   } else if (rq.bRequest == REQUEST_PIC_WRITE_CODE) {
      *(uint16_t*) data = PIC_write_code_area(&buffer[rq.wIndex.word], rq.wValue.word, picCodeWriteSize);
      picCodeWriteSize = 0;
      return 2;
   } else if (rq.bRequest == REQUEST_PIC_VERIFY_CODE) {
      *(uint16_t*) data = PIC_verify_code_area(&buffer[rq.wIndex.word], rq.wValue.word, picCodeWriteSize);
      picCodeWriteSize = 0;
      //*(uint16_t*) data = 0xdead;
      return 2;
   } else if (rq.bRequest == REQUEST_PIC_READ_CODE) {
      *(uint16_t*) data = PIC_read_code_area(&buffer[rq.wIndex.word], rq.wValue.word, picCodeWriteSize);
      picCodeWriteSize = 0;
      return 2;
   } else if (rq.bRequest == REQUEST_PIC_SET_CODE_RW_SIZE) {
      picCodeWriteSize = rq.wValue.word;
      return 0;
   } else if (rq.bRequest == REQUEST_PIC_POWER_ON) {
      PIC_power_on();
      return 0;
   } else if (rq.bRequest == REQUEST_PIC_POWER_OFF) {
      PIC_power_off();
      return 0;
   } else if (rq.bRequest == REQUEST_PIC_GET_CONFIG) {
      if ((rq.wValue.word + rq.wLength.word) <= 14) {
         *data = PIC_read_configuration(&buffer[rq.wIndex.word], rq.wValue.word, rq.wLength.bytes[0]);
         return (*data != 0) ? 1 : 0xFF;
      } else {
         return 0xFF;
      }
   } else if (rq.bRequest == REQUEST_PIC_SET_CONFIG) {
      *data = PIC_write_configuration(&buffer[rq.wIndex.word], rq.wValue.word, rq.wLength.bytes[0]);
      return (*data != 0) ? 1 : 0xFF;
   } else if (rq.bRequest == REQUEST_PIC_GET_IDENTIFICATION) {
      *data = PIC_read_identification(&buffer[rq.wIndex.word], rq.wValue.word, rq.wLength.bytes[0]);
      return 1;
   } else if (rq.bRequest == REQUEST_PIC_SET_IDENTIFICATION) {
      if (rq.wLength.bytes[0] & 1) {
         return 0xFF;
      }
      *data = PIC_write_identification(&buffer[rq.wIndex.word], rq.wValue.word, rq.wLength.bytes[0]);
      return (*data != 0) ? 1 : 0xFF;
   } else if (rq.bRequest == REQUEST_PIC_BULK_ERASE) {
      PIC_perform_bulk_erase();
      data[0] = 65;
      data[1] = 67;
      data[2]++;
      return 8;
#  endif
   }
   return 0xFF;
}

usbMsgLen_t usbFunctionSetup(uchar data[8]) {
   // for some not really clear reason, we need to copy the request data into safe
   // place. I suspect library misbehavior there, but further investigation is to be
   // required. I decided to always copy the data into safe place. It saves me a trouble
   // of managing branches... :)
   memcpy(&rq, (void *) data, 8);

   if ((rq.bmRequestType & USBRQ_TYPE_MASK) == USBRQ_TYPE_CLASS) { /* class request type */
      usbTransferProgress = 0;

      if (rq.bRequest & REQUEST_EXECUTE_COMMAND_MASK) { // requests that has most significant bit on, are function calls.
         if (rq.bmRequestType & USBRQ_DIR_DEVICE_TO_HOST) {  // if this is endpoint IN transaction
#           ifdef HAS_REGISTER_READ
            if (rq.bRequest == REQUEST_COMMAND_PORT_READ) {
               usbMsgPtr = rq.wIndex.word;
               return 1;
            }
#           endif
#           ifdef HAS_PIC_INTERACTIONS
            if (rq.bRequest == REQUEST_PIC_GET_SIGNATURE) {
               return USB_NO_MSG;
            } else if (rq.bRequest == REQUEST_PIC_GET_CONFIG) {
               return USB_NO_MSG;
            } else if (rq.bRequest == REQUEST_PIC_BULK_ERASE) {
               return USB_NO_MSG;
            } else if (rq.bRequest == REQUEST_PIC_SET_IDENTIFICATION) {
               return USB_NO_MSG;
            }
#           endif
         } else {
#           ifdef HAS_REGISTER_WRITE
            if (rq.bRequest == REQUEST_COMMAND_PORT) {
               *((uint8_t *) (rq.wIndex.word)) = rq.wValue.bytes[0];
               return 0;
            } else if (rq.bRequest == REQUEST_COMMAND_PORT_SET) {
               *((uint8_t *) (rq.wIndex.word)) |= rq.wValue.bytes[0];
               return 0;
            } else if (rq.bRequest == REQUEST_COMMAND_PORT_CLR) {
               *((uint8_t *) (rq.wIndex.word)) &= ~(rq.wValue.bytes[0]);
               return 0;
            } else if (rq.bRequest == REQUEST_COMMAND_PORT_TGL) {
               *((uint8_t *) (rq.wIndex.word)) ^= rq.wValue.bytes[0];
               return 0;
            }
#           endif
         }

      } else if ((rq.bRequest == REQUEST_WRITE_TO_BUFFER) ||        //
               (rq.bRequest == REQUEST_READ_FROM_BUFFER) ||         //
               (rq.bRequest == REQUEST_CLEAR_BUFFER) ||             //
               (rq.bRequest == REQUEST_DEVICE_GET_BUFFER_SIZE) ||   //
               (rq.bRequest == REQUEST_DEVICE_GET_SIGNATURE)        //
               ) {

         return USB_NO_MSG;
      }
   }
   return 0xff; /* default for not implemented requests: return no data back to host */
}

/* ------------------------------------------------------------------------- */

int main(void) {
   uchar i;

   //DDRA = 0b11;
   //PORTA = 1;
   for (i = 10; i; i--) {
      _delay_ms(100);
     // PORTA ^= 0b11;
   }

#  if MEMORY_BUFFER_SIZE <= 0xFF
   for (i = 0; i < MEMORY_BUFFER_SIZE; i++) {
      buffer[i] = i;
   }
#  else
   for (uint16_t i = 0; i < MEMORY_BUFFER_SIZE; i++) {
      buffer[i] = i;
   }
#  endif

   usbInit();
   usbDeviceDisconnect(); /* enforce re-enumeration, do this while interrupts are disabled! */
   i = 0;
   while (--i) { /* fake USB disconnect for > 250 ms */
      //wdt_reset(); It's good to have support for WDT (see documentation of recent AVR!)
      _delay_ms(1);
   }
   usbDeviceConnect();
   sei();

   for (;;) { /* main event loop */
      usbPoll();
   }
}

