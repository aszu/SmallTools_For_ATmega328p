/*
 * picif.c
 *
 *  Created on: Jul 17, 2014
 *      Author: andrzejsz
 */

#include "picif.h"

#ifdef HAS_PIC_INTERACTIONS

volatile uint16_t CodeReadWriteOffset = 0;

uint16_t PIC_set_code_base_address(uint16_t newBaseAddress) {
   uint16_t result = CodeReadWriteOffset;
   CodeReadWriteOffset = newBaseAddress;
   return result;
}

uint16_t PIC_get_code_base_address() {
   return CodeReadWriteOffset;
}

uint16_t PIC_read_signature() {
   uint16_t result = 0;
   resetHWforLVP();
   enterLVP();
   performWrite(0, 0x0e, 0x3f);
   performWrite(0, 0x6e, 0xf8);
   performWrite(0, 0x0e, 0xff);
   performWrite(0, 0x6e, 0xf7);
   performWrite(0, 0x0e, 0xfe);
   performWrite(0, 0x6e, 0xf6);
   *((uint8_t*) &result) = performRead(0b1001);
   *((uint8_t*) &result + 1) = performRead(0b1001);
   exitLVP();
   enterHiZ();
   return result;
}

uint8_t PIC_read_configuration(uint8_t * buffer, uint8_t startByte, uint8_t size) {
   uint8_t i = 0;
   if (size == 0) {
      return 0;
   }
   resetHWforLVP();
   enterLVP();
   performWrite(0, 0x0e, 0x30);
   performWrite(0, 0x6e, 0xf8);
   performWrite(0, 0x0e, 0x00);
   performWrite(0, 0x6e, 0xf7);
   performWrite(0, 0x0e, startByte);
   performWrite(0, 0x6e, 0xf6);
   do {
      *buffer = performRead(0b1001);
      buffer++;
      i++;
   } while (i < size);
   exitLVP();
   enterHiZ();
   return size;
}

uint8_t PIC_write_configuration(uint8_t * buffer, uint8_t startByte, uint8_t size) {
   uint8_t i;
   if (size == 0) {
      return 0;
   }
   if (startByte + size > 14) {
      return 0xFF;
   }
   resetHWforLVP();
   enterLVP();
//   PORTA ^= 0b11;
   performWrite(0, 0x8e, 0xa6);
   performWrite(0, 0x8c, 0xa6);

   performWrite(0, 0x0e, 0x30);
   performWrite(0, 0x6e, 0xf8);
   performWrite(0, 0x0e, 0x00);
   performWrite(0, 0x6e, 0xf7);

   for (i = 0; i < size; i++) {
      performWrite(0, 0x0e, startByte + i);
      performWrite(0, 0x6e, 0xf6);
      performWrite(0b1111, buffer[i], buffer[i]);
      performWriteNoop(9);
   }

//   _delay_us(DELAY_P9);
//   _delay_us(DELAY_P11);
//   _delay_us(DELAY_P12);

   exitLVP();
   enterHiZ();
   return size;
}

uint8_t PIC_read_identification(uint8_t * buffer, uint8_t startByte, uint8_t size) {
   uint8_t i = 0;
   if (size == 0) {
      return 0;
   }
   resetHWforLVP();
   enterLVP();
   performWrite(0, 0x0e, 0x20);
   performWrite(0, 0x6e, 0xf8);
   performWrite(0, 0x0e, 0x00);
   performWrite(0, 0x6e, 0xf7);
   performWrite(0, 0x0e, startByte); // startByte should be put here.
   performWrite(0, 0x6e, 0xf6);
   do {
      *buffer = performRead(0b1001);
      buffer++;
      i++;
   } while (i < size);
   exitLVP();
   enterHiZ();
   return size;
}

uint8_t PIC_write_identification(uint8_t * buffer, uint8_t startByte, uint8_t size) {
   uint8_t i;
   if (size == 0) {
      return 0;
   }
   if (size > 8) {
      return 0xFF;
   }
   resetHWforLVP();
   enterLVP();
//   PORTA ^= 0b11;
   performWrite(0, 0x8e, 0xa6);
   performWrite(0, 0x9c, 0xa6);

   performWrite(0, 0x0e, 0x20);
   performWrite(0, 0x6e, 0xf8);
   performWrite(0, 0x0e, 0x00);
   performWrite(0, 0x6e, 0xf7);
   performWrite(0, 0x0e, startByte);
   performWrite(0, 0x6e, 0xf6);

   for (i = 0; i < size - 2; i += 2) {
      performWrite(0b1101, buffer[i + 1], buffer[i]);
   }
   performWrite(0b1110, buffer[i + 1], buffer[i]);

   performWriteNoop(9);

//   _delay_us(DELAY_P9);
//   _delay_us(DELAY_P11);
//   _delay_us(DELAY_P12);

   exitLVP();
   enterHiZ();
   return size;
}

uint16_t PIC_read_code_area(uint8_t * buffer, uint8_t startByte, uint16_t size) {
   uint16_t i = 0;
   if (size == 0) {
      return 0;
   }
   resetHWforLVP();
   enterLVP();
   performWrite(0, 0x0e, CodeReadWriteOffset >> 8);
   performWrite(0, 0x6e, 0xf8);
   performWrite(0, 0x0e, CodeReadWriteOffset & 0xFF);
   performWrite(0, 0x6e, 0xf7);
   performWrite(0, 0x0e, startByte); // startByte should be put here.
   performWrite(0, 0x6e, 0xf6);
   do {
      *buffer = performRead(0b1001);
      buffer++;
      i++;
   } while (i < size);
   exitLVP();
   enterHiZ();
   return size;
}

uint16_t PIC_verify_code_area(uint8_t * buffer, uint8_t startByte, uint16_t size) {
   uint16_t i = 0;
   if (size == 0) {
      return 0;
   }
   //size = size-1; // max address to check is size - 1!
   resetHWforLVP();
   enterLVP();
   performWrite(0, 0x0e, CodeReadWriteOffset >> 8);
   performWrite(0, 0x6e, 0xf8);
   performWrite(0, 0x0e, CodeReadWriteOffset & 0xFF);
   performWrite(0, 0x6e, 0xf7);
   performWrite(0, 0x0e, startByte); // startByte should be put here.
   performWrite(0, 0x6e, 0xf6);
   do {
      uint8_t flashByte = performRead(0b1001);
      if (*buffer != flashByte) {
         *buffer = 0xaa;
         return i;
      } else {
         *buffer = flashByte;
      }
      buffer++;
      i++;
   } while (i < size);
   resetHWforLVP();
   enterLVP();
   exitLVP();
   enterHiZ();
   return 0xFFFF;
}

uint16_t PIC_write_code_area(uint8_t * buffer, uint8_t startByte, uint16_t size) {
   uint16_t i;
   uint8_t j;
   if (size == 0) {
      return 0;
   } else if (size & 1) {
      return 0xff;
   }

   resetHWforLVP();
   enterLVP();

   performWrite(0, 0x8e, 0xa6);
   performWrite(0, 0x9c, 0xa6);

   performWrite(0, 0x0e, CodeReadWriteOffset >> 8);
   performWrite(0, 0x6e, 0xf8);
   performWrite(0, 0x0e, CodeReadWriteOffset & 0xFF);
   performWrite(0, 0x6e, 0xf7);
   performWrite(0, 0x0e, startByte);
   performWrite(0, 0x6e, 0xf6);

   j = startByte;
   i = 0;
   do {
      if ((j & 0b11111) == 0b11110 || i == size - 2) {
         performWrite(0b1110, buffer[i + 1], buffer[i]);
         performWriteNoop(1);
         if ((j & 0b11111111) == 0b11111110) {
            CodeReadWriteOffset++;
            performWrite(0, 0x0e, CodeReadWriteOffset >> 8);
            performWrite(0, 0x6e, 0xf8);
            performWrite(0, 0x0e, CodeReadWriteOffset & 0xFF);
            performWrite(0, 0x6e, 0xf7);
            performWrite(0, 0x0e, 0);
            performWrite(0, 0x6e, 0xf6);
         }
      } else {
         performWrite(0b1101, buffer[i + 1], buffer[i]);
      }
      i += 2;
      j += 2;
   } while (i < size);

//   performWrite(0b1101, buffer[i + 1], buffer[i]);
//   performWrite(0b1110, buffer[i + 1], buffer[i]);
//   performWriteNoop(1);

//   _delay_us(DELAY_P9);
//   _delay_us(DELAY_P10);

   exitLVP();
   enterHiZ();
   return size;
}

void PIC_perform_bulk_erase() {
   resetHWforLVP();
   enterLVP();
   performWrite(0, 0x0e, 0x3c);
   performWrite(0, 0x6e, 0xf8);
   performWrite(0, 0x0e, 0x00);
   performWrite(0, 0x6e, 0xf7);
   performWrite(0, 0x0e, 0x05);
   performWrite(0, 0x6e, 0xf6);
   performWrite(0b1100, 0x3f, 0x3f);
   performWrite(0, 0x0e, 0x3c);
   performWrite(0, 0x6e, 0xf8);
   performWrite(0, 0x0e, 0x00);
   performWrite(0, 0x6e, 0xf7);
   performWrite(0, 0x0e, 0x04);
   performWrite(0, 0x6e, 0xf6);
   performWrite(0b1100, 0x8f, 0x8f);
   performWrite(0, 0, 0);
   performWriteNoop(0);
//   _delay_us(DELAY_P10);
//   _delay_us(DELAY_P11);
   exitLVP();
   enterHiZ();
}

void PIC_power_on() {
   enterHiZ();
   performPowerOn();
}

void PIC_power_off() {
   enterHiZ();
}

#endif
