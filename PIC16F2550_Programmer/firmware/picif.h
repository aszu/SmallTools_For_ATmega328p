/*
 * picif.h
 *
 *  Created on: Jul 17, 2014
 *      Author: andrzejsz
 */

#ifndef PICIF_H_
#define PICIF_H_

#include "features.h"
#include <avr/io.h>

#ifdef HAS_PIC_INTERACTIONS
#include "picwriter.h"

uint16_t PIC_read_signature();
uint8_t PIC_read_configuration(uint8_t * buffer, uint8_t startByte, uint8_t size);
uint8_t PIC_write_configuration(uint8_t * buffer, uint8_t startByte, uint8_t size);

uint8_t PIC_read_identification(uint8_t * buffer, uint8_t startByte,uint8_t size);
uint8_t PIC_write_identification (uint8_t * buffer, uint8_t startByte, uint8_t size);

uint16_t PIC_set_code_base_address(uint16_t newBaseAddress);
uint16_t PIC_get_code_base_address(void);

uint16_t PIC_write_code_area(uint8_t * buffer, uint8_t startByte, uint16_t size);
uint16_t PIC_read_code_area(uint8_t * buffer, uint8_t startByte, uint16_t size);
uint16_t PIC_verify_code_area(uint8_t * buffer, uint8_t startByte, uint16_t size);

void PIC_perform_bulk_erase();

void PIC_power_on();
void PIC_power_off();

#endif

#endif /* PICIF_H_ */
