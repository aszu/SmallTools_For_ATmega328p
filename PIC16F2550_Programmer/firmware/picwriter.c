/*
 * picwriter.c
 *
 *  Created on: Jul 16, 2014
 *      Author: andrzejsz
 */

#include "picwriter.h"

void resetHWforLVP() {
   PIC_DDR |= 1 << PIC_PGC | 1 << PIC_MCLR | 1 << PIC_PGD | 1 << PIC_PGM | 1 << PIC_VDD;
   PIC_PORT &= ~(1 << PIC_PGC | 1 << PIC_MCLR | 1 << PIC_PGD | 1 << PIC_PGM | 1 << PIC_VDD);
   PIC_PORT |= 1 << PIC_VDD;
}

void enterHiZ() {
   PIC_DDR  &= ~(1 << PIC_PGC | 1 << PIC_MCLR | 1 << PIC_PGD | 1 << PIC_PGM | 1 << PIC_VDD);
   PIC_PORT &= ~(1 << PIC_PGC | 1 << PIC_MCLR | 1 << PIC_PGD | 1 << PIC_PGM | 1 << PIC_VDD);
}

void performPowerOn() {
   enterHiZ();
   PIC_DDR |= 1<<PIC_VDD;
   PIC_DDR |= 1<<PIC_MCLR;
   PIC_PORT |= 1<< PIC_VDD;
   PIC_PORT |= 1<< PIC_MCLR;
}

void enterLVP() {
   PIC_PORT |= 1 << PIC_VDD;
   _delay_ms(100);
   PIC_PORT |= 1 << PIC_PGM;
   _delay_us(DELAY_P15);
   PIC_PORT |= 1 << PIC_MCLR;
   _delay_us(DELAY_P12);
}

void exitLVP() {
   PIC_PORT &= ~(1 << PIC_PGD);
   _delay_us(DELAY_P16);
   PIC_PORT &= ~(1 << PIC_MCLR);
   _delay_us(DELAY_P18);
   PIC_PORT &= ~(1 << PIC_PGM);
   _delay_ms(100);
   PIC_PORT &= ~(1 << PIC_VDD);
   _delay_ms(100);
}

void emitBit(uint8_t value) {
   PIC_PORT |= 1 << PIC_PGC;
   if (value) {
      PIC_PORT |= 1 << PIC_PGD;
   } else {
      PIC_PORT &= ~(1 << PIC_PGD);
   }
   _delay_us(DELAY_P2B);
   PIC_PORT &= ~(1 << PIC_PGC);
   _delay_us(DELAY_P2A);
}

void emitCommand(uint8_t cmd) {
   uint8_t i;
   for (i = 0; i < 4; i++) {
      emitBit(cmd & 1);
      cmd >>= 1;
   }
}

void emitByte(uint8_t cmd) {
   uint8_t i;
   for (i = 0; i < 8; i++) {
      emitBit(cmd & 1);
      cmd >>= 1;
   }
}

uint8_t readBit() {
   uint8_t result = 0;
   PIC_PORT |= 1 << PIC_PGC;
   _delay_us(DELAY_P2B);
   if (PIC_PIN & (1 << PIC_PGD)) {
      result = 1;
   }
   PIC_PORT &= ~(1 << PIC_PGC);
   _delay_us(DELAY_P2A);
   return result;
}

uint8_t readByte() {
   uint8_t i;
   uint8_t result = 0;
   PIC_DDR &= ~(1 << PIC_PGD);
   _delay_us(DELAY_P2B);
   for (i = 0; i < 8; i++) {
      result = (result >> 1);
      result |= readBit() << 7;
   }
   PIC_DDR |= 1 << PIC_PGD;
   return result;
}

void performWrite(uint8_t cmd, uint8_t hiByte, uint8_t loByte) {
   emitCommand(cmd);
   _delay_us(DELAY_P5);
   emitByte(loByte);
   emitByte(hiByte);
   _delay_us(DELAY_P5A);
}

uint8_t performRead(uint8_t cmd) {
   uint8_t result;
   emitCommand(cmd);
   _delay_us(DELAY_P5);
   emitByte(0);
   _delay_us(DELAY_P6);
   result = readByte();
   _delay_us(DELAY_P5A);
   return result;
}

void performWriteNoop(uint8_t useP9timing) {
   uint8_t i;
//   PORTA ^= 1;
   for (i = 0; i < 3; i++) {
      emitBit(0);
   };

   PIC_PORT |= 1 << PIC_PGC;
   PIC_PORT &= ~(1 << PIC_PGD);

   if (useP9timing) {
   _delay_us(DELAY_P9);
   } else {
      _delay_us(DELAY_P11);
   }
   PIC_PORT &= ~(1 << PIC_PGC);
   _delay_us(DELAY_P10);

   emitByte(0);
   emitByte(0);

}
;
