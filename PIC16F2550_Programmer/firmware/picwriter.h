/*
 * picwriter.h
 *
 *  Created on: Jul 16, 2014
 *      Author: andrzejsz
 */

#include <avr/io.h>
#include <util/delay.h>

#ifndef PICWRITER_H_
#define PICWRITER_H_


#define PIC_DDR         DDRC
#define PIC_PORT        PORTC
#define PIC_PIN         PINC
#define PIC_VDD         0
#define PIC_MCLR        1
#define PIC_PGM         2
#define PIC_PGD         3
#define PIC_PGC         4

#define DELAY_P1        1
#define DELAY_P2        1
#define DELAY_P2A       1
#define DELAY_P2B       1
#define DELAY_P3        1
#define DELAY_P4        1
#define DELAY_P5        1
#define DELAY_P5A       1
#define DELAY_P6        1
#define DELAY_P9        1000
#define DELAY_P10       100
#define DELAY_P11       5000
#define DELAY_P11A      4000
#define DELAY_P12       2
#define DELAY_P13       1
#define DELAY_P14       1
#define DELAY_P15       2
#define DELAY_P16       1
#define DELAY_P17       1 // max 100ns... Probably this need to be set to 0.
#define DELAY_P18       1


void resetHWforLVP ();
void enterLVP();
void enterHiZ();
void exitLVP();
void performPowerOn();
void performWrite(uint8_t, uint8_t, uint8_t);
uint8_t performRead(uint8_t);

void performWriteNoop(uint8_t withPayload);

#endif /* PICWRITER_H_ */
