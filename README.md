This directory will contain a number of tools I've written to run on ATmega328p
(and similar, most will also run on ATmegas 8, 8A, 16A and number of others with
small changes - or even without ones).

The project will contain for sure:
 - ATtiny85 fuse resetter (I need that for Digispark "hacking"),
 - PIC16F2550 programmer (written for ATmega - I've wanted to have some fun
   with PIC family and haven't wished to buy PIC programmer),
 - 18* channel USB PWM driver for LEDs (that was pretty nice to do),<br>
   (*it was 24 channel when was developed for ATmega16A, but here IO pin count
   limits what we can do)
 - and some more...
 
I treat most of the code I write as prototypes, so the working is more important
than optimizing those. Who cares whether programming PIC takes 5 or 35 seconds? ;)

Drop an issue here if you want to have this happened sooner. :)