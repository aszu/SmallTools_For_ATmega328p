#!/usr/bin/perl -w
use common::sense;

# WORKAROUND FOR Device::USB stupid bug that caused throwing warnings to console.
# Let it be here. Should not be dangerous.
BEGIN {
   $ENV{CFLAGS} = ''    unless defined $ENV{CFLAGS};
   $ENV{CPPFLAGS} = ''  unless defined $ENV{CPPFLAGS};
   $ENV{LDFLAGS} = ''   unless defined $ENV{LDFLAGS};
};

use Data::Dumper;
$Data::Dumper::Sortkeys = 1;

# see /usr/include/usb.h (on most setups) for details... :)
use constant {
USB_REQ_GET_STATUS            =>  0x00,
USB_REQ_CLEAR_FEATURE         =>  0x01,
USB_REQ_SET_FEATURE           =>  0x03,
USB_REQ_SET_ADDRESS           =>  0x05,
USB_REQ_GET_DESCRIPTOR        =>  0x06,
USB_REQ_SET_DESCRIPTOR        =>  0x07,
USB_REQ_GET_CONFIGURATION     =>  0x08,
USB_REQ_SET_CONFIGURATION     =>  0x09,
USB_REQ_GET_INTERFACE         =>  0x0A,
USB_REQ_SET_INTERFACE         =>  0x0B,
USB_REQ_SYNCH_FRAME           =>  0x0C,
USB_TYPE_STANDARD             =>  (0x00 << 5),
USB_TYPE_CLASS                =>  (0x01 << 5),
USB_TYPE_VENDOR               =>  (0x02 << 5),
USB_TYPE_RESERVED             =>  (0x03 << 5),
USB_RECIP_DEVICE              =>  0x00,
USB_RECIP_INTERFACE           =>  0x01,
USB_RECIP_ENDPOINT            =>  0x02,
USB_RECIP_OTHER               =>  0x03,
USB_ENDPOINT_IN               =>  0x80,
USB_ENDPOINT_OUT              =>  0x00,

#/* Error codes */
USB_ERROR_BEGIN               =>  500000,

};


use Device::USB;

my $usb = Device::USB->new;
my $dev =$usb->find_device( 0x6666, 0x30ae )
   or die "Unable to find device!";

$dev->open() or die " ups ";
printf STDERR "Device: %04X:%04X opened.\n", $dev->idVendor(), $dev->idProduct();

my $counter = 10;

$ARGV[0] = uc $ARGV[0];

if ($ARGV[0] eq 'IRQ_RECV') {

   while ($counter--) {
      my $xres;


      my $buffer = '        ';
      $xres = $dev->interrupt_read(
         1, # endpoint no
         $buffer,
         8,
         500
         );

      printf ("Result: %d, buffer(%s)\n", $xres, $buffer);

   }

} elsif ($ARGV[0] eq 'LED_CTRL') {
   my $action = 1;
   while ($action) {
      my $cmd = '';
      $cmd = uc <STDIN>;
      chomp $cmd;
      print "Command: $cmd.\n" if $cmd;
      if ($cmd eq '') {
         $action = 0;
      } else {
         my $res = sendCmd($dev, 0, $cmd, 8);
         printf "Result: %d\n", $res;
      }
   }
} elsif ($ARGV[0] eq 'KITT') {
   my $action = 1;
   my @ledlevels = (9,0,0,0,0,0,0,0,   0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0  );
   my $direction = 1;
   while ($action) {
      ## then, recalculate new levels.
      for (my $i = 0; $i<@ledlevels; $i++) {
         if ($ledlevels[$i] == 9) {
            if ($direction  == 1) {
               if ($i < $#ledlevels) {
                  $ledlevels[$i+1] = 10;
               } else {
                  $direction  = -$direction;
                  $ledlevels[$i]= 10;
               }
            } else {  #direction == -1
               if ($i > 0) {
                  $ledlevels[$i-1] = 10;
               } else {
                  $direction  = -$direction;
                  $ledlevels[$i]= 10;
               }
            }
         }
      }

      #update levels. if level changed - send new to device.

      for (my $i = 0; $i<@ledlevels; $i++) {
         if ($ledlevels[$i] > 0) {
            $ledlevels[$i]--;
            my $port = chr( int($i/8)+65 );
            my $cmd = "$port".($i%8)."$ledlevels[$i]";
            exit if sendCmd($dev, 0, $cmd, 8) < 0;
         }
      }
      # sleep
      #select (undef, undef, undef, 0.04);
   }
} else {
   print <<HELPMSG
   Use parameter!
      IRQ_RECV to check receiving usb interrupts.
      LED_CTRL to control device leds
      KITT     to control device leds automagically :)
HELPMSG
}

sub sendCmd {
   my ($dev, $rqType, $cmd, $size) = @_;

my $buf;
         $buf = substr $cmd, 0, $size;
         $buf .= ("\0" x ($size- length $buf));

  my $res =   $dev->control_msg(
            USB_TYPE_CLASS | USB_RECIP_DEVICE | USB_ENDPOINT_OUT,
            $rqType, # request type
            0x0000,
            0,
            $buf,
            $size,
            0
            );

   return $res;
}

__END__

