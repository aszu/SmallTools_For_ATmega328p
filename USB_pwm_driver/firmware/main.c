/*
 * main.c
 *
 *  Created on: Jun 21, 2014
 *      Author: andrzejsz
 */
#ifndef uchar
#define uchar   unsigned char
#endif

/*
 * Application can be built for Arduino (uses TX/RX of Atmega, connecting to some
 * kind of USB-to-serial converter) or for V-usb (uses D2 and D4 for custom USB
 * interface based on Vusb). Define one of the following.
 *
 * If you define MODE_ARDUINO, uncomment 'OBJECTS = main.o' line in
 */
#ifndef MODE_ARDUINO
#ifndef MODE_VUSB

//#define MODE_ARDUINO
#define MODE_VUSB

#endif
#endif

/*
 * In version for ATmega328P we have avaliable following channels:
 * B 0,1,2,3,4,5
 * C 0,1,2,3,4,5
 * D 0,1,3,5,6,7 (in vusb mode)
 * D 2,3,4,5,6,7 (in arduino mode)
 * This totals to 18 channels available for PWM driving.
 */
#define BMASK 0b11000000
#define CMASK 0b11000000
#ifdef MODE_VUSB
#define DMASK 0b00010100
#endif
#ifdef MODE_ARDUINO
#define DMASK 0b00000011
#endif


#include <avr/io.h>
#include <avr/wdt.h>
#include <avr/interrupt.h>
#include <util/delay.h>
//#include "usbdrv/usbdrv.h"


#ifdef MODE_VUSB
#include <avr/pgmspace.h>   /* required by usbdrv.h */
#include "usbdrv/usbdrv.h"
#endif
#ifdef MODE_ARDUINO
#define BAUD 9600
#include <util/setbaud.h>
#endif
//#include "requests.h"


#ifdef MODE_ARDUINO

#define INBUFSIZE 8
uchar data[INBUFSIZE];
volatile uint8_t dataIndex = 0;

#define OUTBUFSIZE 16
uint8_t outBuf[OUTBUFSIZE];
volatile uint8_t outBufSent = 0;
volatile uint8_t outBufTail = 0;


#endif

/* ------------------------------------------------------------------------- */
/* ----------------------------- USB interface ----------------------------- */
/* ------------------------------------------------------------------------- */

#ifdef MODE_VUSB
volatile uint8_t usbOn = 1;
#endif

// for the "PWM" be fast enough, we need to "precalculate" each channel on/off times.
// this way, we do not need any comparisions inside the "refresh led loop" - we already
// know the results... Yes, that might be a RAM waster. Yes, I do not know avr assembler.
uint8_t PWMD[256];
uint8_t PWMB[256];
uint8_t PWMC[256];

uint8_t getHexValue(uchar c) {
   if (c < 0x40) {
      return c - 0x30;
   } else {
      return c - 55;
   }
}

void processBuffer(uchar *data) {
	   uint8_t scaledLevel;
	   uint8_t bitNo;
	   uint8_t tmp3;
	   uint8_t * p1;

	   if (data[0] < 69 && data[0] > 65) { // if this is either D or B or C
	      p1 = PWMD; // just to keep one warning shut... :)

	      // first, make data[1] keep led number
	      data[1] = getHexValue(data[1]);

	      // make data[2] keep the value (0..9)
	      data[2] = getHexValue(data[2]);

	      // then, make tmp1 to keep "scaled value" - as human perception of
	      // light is not linear, we will do such a trick to trick the eyes... :)
	      if (data[2] == 0) {
	         scaledLevel = 0;
	      } else if (data[2] == 9) {
	         scaledLevel = 0xFF;
	      } else {
	         scaledLevel = 1 << (data[2] - 1);
	      }

	      // tmp2 is going to hold precalculated bit shift value for updating
	      // PWMA table.
	      bitNo = 1 << data[1];

	      // we also need to know which table to update... :)
	      switch (data[0]) {
	      case 'D':
	         p1 = PWMD;
	         break;
	      case 'B':
	         p1 = PWMB;
	         break;
	      case 'C':
	         p1 = PWMC;
	         break;
	      }

	      tmp3 = 0;
	      if (scaledLevel != 0) {
			  while (tmp3<=scaledLevel) {
				  p1[tmp3] |= bitNo;
				  tmp3++;

				  // if we did a full loop, break the loop.
				  if (!tmp3){
					  break;
				  }
			  }
	      }
	      // if scaledLevel was 0xFF, the tmp3 will be 0 here, so for 0xFF no "bit to zero setting" should occur.
	      // other case to set anything to zero (everything, to be precise...) is when scaledLevel is equal to 0
	      if (tmp3 || (scaledLevel == 0)) {
              bitNo = ~bitNo;
			  do {
				 p1[tmp3] &= bitNo;
				 tmp3++;
			  } while (tmp3);
	      }
	   } else if (data[0] == 'U') {
#ifdef MODE_VUSB
	      usbOn = 2;
#endif
	   } else if (data[0] == 'F') {
	//	    scaledLevel = getHexValue(data[1]);
	//      bitNo = getHexValue(data[2]);
	//      OCR2 = scaledLevel << 4 | bitNo;
	   }

}

#ifdef MODE_VUSB
uchar usbFunctionWrite(uchar *data, uchar len) {

   processBuffer(data);

   return 1;
}

usbMsgLen_t usbFunctionSetup(uchar data[8]) {
   usbRequest_t *rq = (void *) data;
//
//    /* The following requests are never used. But since they are required by
//     * the specification, we implement them in this example.
//     */
   if ((rq->bmRequestType & USBRQ_TYPE_MASK) == USBRQ_TYPE_CLASS) { /* class request type */
      if (rq->bRequest == 0) {
         return USB_NO_MSG;
         /* wValue: ReportType (highbyte), ReportID (lowbyte) */
         /* we only have one report type, so don't look at wValue */
//        }else if(rq->bRequest == USBRQ_HID_GET_IDLE){
//        }else if(rq->bRequest == USBRQ_HID_SET_IDLE){
//        }
//    }else{
//        /* no vendor specific requests implemented */
      } else {
      }
   }
   return 0xff; /* default for not implemented requests: return no data back to host */
}

#endif
#ifdef MODE_ARDUINO
void sendByte(uint8_t b) {
	// TODO: optimize outbufsize - it might have used bitwise ops.
	outBufTail = (outBufTail + 1) % OUTBUFSIZE;
	outBuf[outBufTail] = b;
	if (UCSR0A & (1<<UDRE0)) {
		UDR0 = b;
		outBufSent = (outBufSent + 1) % OUTBUFSIZE;
	}
}

ISR(USART_RX_vect) {

	// This is not necessary, but it is here to force read of UDR even when INBUF is
	// overfilled.
	// It also improves readability a bit.
	uint8_t byte = UDR0;

	if (dataIndex >= INBUFSIZE) {
		sendByte('.');
	} else {
		data[dataIndex++] = byte;
	}

	if ((byte == 10) || (byte == 13)) {
		dataIndex = 0;
		processBuffer(data); // ugly, should not be called from interrupt.
		sendByte(']');
		sendByte(13);
		sendByte(10);
	}
}

ISR(USART_TX_vect) {
	if (outBufSent != outBufTail) {
		outBufSent = (outBufSent+1) % OUTBUFSIZE;
		UDR0 = outBuf[outBufSent];
	}
}
#endif

register uint8_t isrCnt asm("r2");

ISR(TIMER0_COMPA_vect) {
//   TIMSK &= ~(1 << OCIE2);
//   sei();
   isrCnt++;

   PORTB = (PORTB & (BMASK) ) | (PWMB[isrCnt] & (~BMASK));
   PORTC = (PORTC & (CMASK) ) | (PWMC[isrCnt] & (~CMASK));
   PORTD = (PORTD & (DMASK) ) | (PWMD[isrCnt] & (~DMASK));
//   TIMSK |= 1 << OCIE2;
}


#ifdef MODE_ARDUINO
void uartInit() {
	UBRR0H = UBRRH_VALUE;
	UBRR0L = UBRRL_VALUE;
	UCSR0B = 0b11011000;
	UCSR0C = 0b00000110;
}
#endif


/* ------------------------------------------------------------------------- */

uint16_t counter = 0;
uint8_t buffer[8] =
   { 0, 0, 0, 0, 0, 0, 0, 0 };
uint16_t workbuffer = 0;

int main(void) {
   uchar i;

   i = 0;
   do {
      PWMD[i] = 0x0;
      PWMB[i] = 0x0;
      PWMC[i] = 0x0;
      i++;
   } while (i);

   isrCnt = 1;
//
   DDRB |= ~BMASK;
   DDRC |= ~CMASK;
   DDRD |= ~DMASK;

   /* If we are in vusb mode, initialize vusb library... */
#ifdef MODE_VUSB

   usbInit();
   usbDeviceDisconnect(); /* enforce re-enumeration, do this while interrupts are disabled! */
   i = 0;
   while (--i) { /* fake USB disconnect for > 250 ms */
      wdt_reset();
      _delay_ms(1);
   }
   usbDeviceConnect();
#endif
#ifdef MODE_ARDUINO
   uartInit();
#endif


   sei();

   // MODE: CTC, counts to OCR2, OC disconnected, prescaling changed from 32 to 8
   TCCR0A = 0b00000010;
   TCCR0B = 0b00000010;
   OCR0A = 64;
   // enable timer/counter compare interrupt
   TIMSK0 = 1 << OCIE0A;

   for (;;) { /* main event loop */
#ifdef MODE_VUSB
      if (usbOn) {
         usbPoll();
      }
      if (usbOn == 2) {
         usbOn = 0;
      }
#endif
   }
}

/* Set interrupt example:
 * called after every poll of the interrupt endpoint
 *
 if(usbInterruptIsReady()){
 workbuffer = counter++;
 buffer[6]=0x30+(workbuffer % 10); workbuffer = workbuffer/10;
 buffer[5]=0x30+(workbuffer % 10); workbuffer = workbuffer/10;
 buffer[4]=0x30+(workbuffer % 10); workbuffer = workbuffer/10;
 buffer[3]=0x30+(workbuffer % 10); workbuffer = workbuffer/10;
 buffer[2]=0x30+(workbuffer % 10); workbuffer = workbuffer/10;
 buffer[1]=0x30+(workbuffer % 10); workbuffer = workbuffer/10;
 buffer[0]=0x30+(workbuffer % 10);
 usbSetInterrupt(buffer,7);
 } else {
 _delay_ms(450);
 }
 *
 *
 */

/* ------------------------------------------------------------------------- */
