#!/usr/bin/node

console.info("megaAVR boot flasher");

try {
var serialport = require('serialport');
} catch (e) {
   console.info("Something went wrong: "+e);
   console.info(" ** have you already run: npm install ?? ** \n\n");
   return;
}

//console.info(process);


var portDesignation = process.argv[2];

var port = new serialport.SerialPort(portDesignation);

port.on('open', function (err) {
      if (err) {
         return console.err(err);
      }
      console.info('Port opened');
      var li = new LoaderInteractor(port);
      li.run();
});



function LoaderInteractor(port) {

   this.run = function () {
      console.info("Starting business.");
      this.iStatus = this.STATE_WAITING_FOR_SIGNATURE;
      var scope = this;
      port.on('data', function(data) { scope.onDataReceived(data) });
   }

   this.onDataReceived = function(data) {
      console.info("DTA RCV: ", data, data.length);
      switch (this.iStatus) {
      case this.STATE_WAITING_FOR_SIGNATURE:
         if (this.checkForSignature(data)) {
            this.sendEnterBootmodeCommand();
         }
         break;
      case this.STATE_SEND_ENTER_BOOTMODE_REQUEST:
         if (this.readBootloaderDescription(data)) {
            console.info(this.hDeviceDescriptor);
         }
         break;
      default:
         throw "Whooo... State not known?";
      }
   }


   this.checkForSignature = function(data) {
      if ( typeof(arguments.callee.str) == 'undefined') {
         arguments.callee.str = '';
      }
      var stringFromBuffer = '';
      for (var i = 0; i < data.length; i++) {
         var ch = String.fromCharCode(data.readUInt8(i));
         switch(ch) {
         case '\n':
            stringFromBuffer = arguments.callee.str;
            arguments.callee.str = '';
            break;
         case '\r':
            break;
         default:
            arguments.callee.str += ch;
         }
      }

      console.info("Got string: "+stringFromBuffer);
      if(stringFromBuffer === 'BOOT') {
         delete arguments.callee.str;
         return true;
      } else {
         return false;
      };
   }

   this.readBootloaderDescription = function(data) {
      /*           SendByte('+');
                // Send header to the host
                SendByte( 0x01 ); // Bootloader version
                SendByte( SIGN ); // Hardware signature (see makefile)
                SendByte( SPM_PAGESIZE );
                SendByte( BLS_START >> 8);
                SendByte( boot_lock_fuse_bits_get( GET_LOW_FUSE_BITS  ) );  //Fusebity
                SendByte( boot_lock_fuse_bits_get( GET_HIGH_FUSE_BITS ) );
                SendByte( boot_lock_fuse_bits_get( GET_LOCK_BITS      ) );
                SendByte( '+' );
                */
       if (typeof(arguments.callee.buffer) == 'undefined') {
          arguments.callee.buffer = [];
       }
       var buffer = arguments.callee.buffer;

       for (var i = 0; i < data.length; i++) {
          buffer[buffer.length] = data.readUInt8(i);
       }

       // first of all, there is a point to check anything if we know the version of the
       // bootloader we are talking to.
       if (buffer.length > 1) {
          if (buffer[1] == 1) {
             if (buffer.length == 9) {
                this.hDeviceDescriptor = {
                   bootloaderVersion: buffer[1],
                   deviceSignature  : buffer[2],
                   spmPageSize      : buffer[3],
                   bootloaderAddress: buffer[4]<<8,
                   fuseBits: {
                      low: buffer[5],
                      high: buffer[6],
                      lock: buffer[7]
                   }
                };
                return true;
             } else {
                return false;
             }
          } else {
             throw "unsupported Bootloader version: "+buffer[1];
          }
       } else {
          return false;
       }

   }


   this.sendEnterBootmodeCommand = function() {
      this.iStatus = this.STATE_SEND_ENTER_BOOTMODE_REQUEST;
      port.write('s');
   }

   this.STATE_WAITING_FOR_SIGNATURE = 0;
   this.STATE_SEND_ENTER_BOOTMODE_REQUEST = 1;
}

