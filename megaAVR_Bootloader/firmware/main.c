/*

'Aszu's bootloader.

Started of bootloader by "mlodedrwale".

 */

// Ustawienia uart
//#define BAUD 57600
//#define BAUD_PRESCALE ((F_CPU + BAUD * 8L) / (BAUD * 16L) - 1)
#include <util/setbaud.h>

#ifndef UCSRA
#define UCSRA UCSR0A
#endif

#ifndef UCSRB
#define UCSRB UCSR0B
#endif

#ifndef UCSRC
#define UCSRC UCSR0C
#endif

#ifndef UDRE
#define UDRE UDRE0
#endif

#ifndef UDR
#define UDR UDR0
#endif

#ifndef RXC
#define RXC RXC0
#endif

#ifndef UBRRL
#define UBRRL UBRR0L
#endif
#ifndef UBRRH
#define UBRRH UBRR0H
#endif

#ifndef RXEN
#define RXEN RXEN0
#endif
#ifndef TXEN
#define TXEN TXEN0
#endif

#ifndef UCSZ1
#define UCSZ1 UCSZ01
#endif
#ifndef UCSZ0
#define UCSZ0 UCSZ00
#endif

#ifndef URSEL
#define URSEL URSEL01
#endif


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//Libraries
#include <avr/boot.h>
#include <avr/pgmspace.h>
#include <avr/eeprom.h>
#include <util/delay.h>
#include <avr/wdt.h>




/* ADDR_UNION is one of the most important size-tricks here - it's sole purpose is to limit use
 * of the << and >> operators - using it, we just have access to sth.bytes.hi, sth.bytes.lo, sth.word
 * and it helps compiler to produce small code.
 */
typedef struct {
  uint8_t lo;
  uint8_t hi;
} ADDRESS_SEP;

typedef union {
  ADDRESS_SEP bytes;
  uint16_t    word;
} ADDR_UNION;


// This is how we will jump to code after bootloader finishes.
static void (*jump_to_app)(void) = 0x0000;

// Send byte via UART
static void SendByte( const char Data )
{
  while( !( UCSRA & (1<<UDRE) ) );
  UDR = Data;
}

// Receive byte from UART
static uint8_t ReadByte(void)
{
  while ( !( UCSRA & (1<<RXC) ) );
  return UDR;
}

static uint8_t ReadByteWait(void){
  uint8_t Timeout=200;
  do{
    if ( UCSRA & 1 << RXC ) {
      return UDR;
    }
    _delay_loop_2(0);
  } while ( --Timeout );
  return 0xFF;
}

// Send newline (!dos newline)
inline static void SendNL(void) {
  SendByte('\r');
  SendByte('\n');
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Various technical stuff.
static void skip(void) __attribute__ ((naked, section (".vectors") ));
static void skip(void)
{
  asm volatile("rjmp setup");
}

static void setup(void) __attribute__ ((naked, used, section (".init2") ));
static void setup(void)
{


  //Inicjacja stosu i rejestru statusu, daje r�wnie� czas dla uart na inicjalizacj�
  asm volatile ( "clr __zero_reg__" );
  SREG = 0;   // Ustawiamy rej statusu
  SP = RAMEND; // i wsk stosu
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
void __attribute__ ((naked, section (".init9"),  used, noreturn )) main(void)
        {
  //Initialize UART
  UBRRL = UBRRL_VALUE;
  UBRRH = UBRRH_VALUE;
  UCSRB |= (1<<RXEN)|(1<<TXEN);

  wdt_disable();

  // Data format: 8bit, no parity, 1 stop bit.

  // Send signature, wait for something sent back.
  SendByte( 'B' );
  SendByte( 'O' );
  SendByte( 'O' );
  SendByte( 'T' );
  SendNL();


  //Sprawdzenie je�eli nie ma odpowiedzi lub r�ni si� ona od 's' to bootloader skacze do programu



  uint8_t test;
  do {
    test = ReadByteWait();
  } while (test == 10 || test == 13);

  if( test == 's')
  {
    SendByte('+');
    // Send header to the host
    SendByte( 0x01 ); // Bootloader version
    SendByte( SIGN ); // Hardware signature (see makefile)
    SendByte( SPM_PAGESIZE );
    SendByte( BLS_START >> 8);
    SendByte( boot_lock_fuse_bits_get( GET_LOW_FUSE_BITS  ) );  //Fusebity
    SendByte( boot_lock_fuse_bits_get( GET_HIGH_FUSE_BITS ) );
    SendByte( boot_lock_fuse_bits_get( GET_LOCK_BITS      ) );
    SendByte( '+' );

    uint8_t command;

    // Main loop comes here...

    while ( 1 )
    {
      // Read order
      command = ReadByte();

      ADDR_UNION address;
      address.bytes.lo = ReadByte();
      address.bytes.hi = ReadByte();

      // === F: STORE FLASH MEMORY ===
      if (command=='F')
      {
        uint8_t crc = 0xFF;
        // just wait to be sure we can go with flash/eeprom operations.
        boot_spm_busy_wait();

        // Receive address (remember the trick with union)
        uint16_t pageAddress;
        pageAddress = address.word;

        crc^=address.bytes.lo;
        crc^=address.bytes.hi;

        if( pageAddress >= BLS_START ) break;
        // Erase flash page
        boot_page_erase( pageAddress );
        boot_spm_busy_wait();

        //Send "ready for data" sign.
        SendByte( '>' );

        //Put data into page buffer...
        for ( uint16_t Byte_Address = 0; Byte_Address < SPM_PAGESIZE; Byte_Address += 2 )
        {
          // get the instruction, calculate next value for checksum
          address.bytes.lo  = ReadByte();
          address.bytes.hi  = ReadByte();
          crc^=address.bytes.lo;
          crc^=address.bytes.hi;
          boot_page_fill( Byte_Address, address.word );
        }
        // send checksum for client - for verification, wait for status from flashing host.
        SendByte(crc);
        // If checksum is OK flasher should respond with 'k'. If so, proceed...
        if(ReadByte()=='k')
        {
          boot_page_write( pageAddress );
          boot_spm_busy_wait();
        }

      }else {//flash written.


        // === f : read flash memory ===
        if(command=='f')
        {
//          ADDR_UNION address;
//          address.bytes.lo = ReadByte();
//          address.bytes.hi = ReadByte();
          //uint16_t address = ( ( uint16_t )ReadByte() + (ReadByte()<<8));
          uint8_t cnt = SPM_PAGESIZE;
          while(cnt--)
          {
            SendByte( pgm_read_byte_near( address.word-cnt ) );
          }
        }else {

          // === READ EEPROM BYTE ===
          if(command=='e')
          {
//            ADDR_UNION address;
//            address.bytes.lo = ReadByte();
//            address.bytes.hi = ReadByte();
            SendByte(eeprom_read_byte((uint8_t *)(address.word)) );
          }else {

            // === STORE EEPROM BYTE ===
            if(command=='E')
            {
//              ADDR_UNION address;
//              address.bytes.lo = ReadByte();
//              address.bytes.hi = ReadByte();
              eeprom_write_byte((uint8_t *)(address.word), ReadByte() );
            }else{
              break;
            };
          }
        }
      }
    }
  }
  SendByte( 'Q' );
  SendNL();
  SendNL();
  //_delay_ms(10);
  //UCSRB = 0;
  boot_rww_enable_safe();
  jump_to_app();
  while(1);
        }
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//EOF
